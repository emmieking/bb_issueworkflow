"""
development_dummy.txt
================================
.. code-block:: XML

    Purpose:    Dummy file to test BB issue tracker workflow.
    Author:     Emmie King, Emmie@see.com
    Project:    FDS
    NDA/ITAR:   None
    Software:   Developed Python v.3.7
    Version:    1, release date TBD
    Input:      None
    Output:     None
    Copyright:  Space Exploration Engineering 2020
    
"""

def OrbitEnergyFromCartState(rMag, vMag, Gm):
    """ Summary Line....

        Extended description of function.

    
        Parameters
        ----------
        rMag:   (m)
             scalar position magnitude 
        vMag:   (m/s)  
            scalar velocity magnitude
        Gm:     (m^3/sec^2)    
            gravitational param 


        Yields
        -------
        Orbit Energy:   (m^2/s^2)
            orbit energy from Cartesian state 


        Raises
        ------
        ZeroDivisionError
            if ``rMag`` is equal to 0

    """

    if rMag == 0:
        raise ZeroDivisionError('Can not divide by zero')

    return (vMag**2 / 2) - Gm / rMag
